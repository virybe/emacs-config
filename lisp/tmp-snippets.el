;; (require 'yasnippet)
;; ;; from: https://stackoverflow.com/a/10366711
;; (defun yas-insert-by-name (name)
;;   (cl-flet ((dummy-prompt
;;           (prompt choices &optional display-fn)
;;           (declare (ignore prompt))
;;           (or (find name choices :key display-fn :test #'string=)
;;               (throw 'notfound nil))))
;;     (let ((yas-prompt-functions '(dummy-prompt)))
;;       (catch 'notfound
;;         (yas-insert-snippet t)))))


;; (defun my/new-file (lang)
;;   (let ((filename (concat "main." lang)))
;;     (progn
;;       (cd (make-temp-file "tmp." t))
;;       (make-empty-file filename)
;;       (find-file filename)
;;       (yas-insert-by-name (concat lang "-main"))
;;       (save-buffer)
;;       (revert-buffer nil t)
;;       )))

;; (setq lang-options '(("go") ("c") ("py") ("cpp")))

;; (defun my/new-temp (lang)
;;   (interactive
;;    (list (completing-read "Choose: " lang-options)))
;;   (my/new-file lang ))


(provide 'tmp-snippets)
