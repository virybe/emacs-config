(require 'transient)

;;;;###autoload

(defvar elf-current-page 'main)

(defun show-page (page)
  (setq elf-current-page page)
  (pcase page
   ('main (elf-main))
   ('r (elf-raw))
   ('h (elf-dispatch))
   ('q (quit-window))

   ('all (show-cmd-output "readelf --all %s"))
   ('gg (show-cmd-output "readelf --debug-dump %s"))
   ('b (show-cmd-output "hexdump -C %s"))
   ('ldd (show-cmd-output "ldd %s"))

   ('ss (show-cmd-output "readelf --demangle --symbols %s"))
   ('sd (show-cmd-output "readelf --dyn-syms %s"))
   ('sl (show-cmd-output "readelf --lto-syms %s"))
   )
  )

(defmacro ppage (page)
  `(lambda ()
    (interactive)
    (show-page ,page)))

(defun insert-string-readonly (&rest s)
  (let ((inhibit-read-only t))
    (progn
      (delete-region (point-min) (point-max))
      (insert (string-join s "\n"))
      (set-buffer-modified-p nil)
      (read-only-mode 1)
      (beginning-of-buffer)
      )))

(defun show-cmd-output (cmd)
  (let* ((formatted-cmd (format cmd (buffer-file-name)))
         (shell-cmd-output (shell-command-to-string formatted-cmd)))
    (insert-string-readonly (format "cmd:\t%s\n" formatted-cmd) (string-trim shell-cmd-output))))

(defun elf-raw ()
  (interactive)
  (let ((inhibit-read-only t))
    (progn
      (erase-buffer)
      (insert-file-contents (buffer-file-name))))
  (read-only-mode 1)
  (beginning-of-buffer))


(defun elf-main ()
  (interactive)
  (insert-string-readonly
   (format "filename: %s\n" (buffer-file-name))
   (format "Info:\n%s\n" (concat "- " (string-replace ", " "\n- " (shell-command-to-string
                                   (format "file --brief %s" (buffer-file-name))))))
   "Keybindings:
- [a] Show all ELF sections
- [g] Debug info
- [d] Disassemble
- [s] Symbols
- [r] Raw file
- [l] ldd
- [b] Binary
- [h] Help
- [q] Quit"))


(transient-define-prefix elf-debug-prefix ()
  "Prefix for showing debug informations"
  [
   ("g" "Display the contents of ELF debug sections" (lambda () (interactive) (show-page 'gg)))
   ])

(defun elf-disassemble-suffix (the-prefix-arg)
  "TODO: doc"
  :transient 'transient--do-call
  (interactive "P")
  (let ((args (transient-args (oref transient-current-prefix command))))
    (show-cmd-output (concat "objdump -C -d " (string-join args " ") " %s"))))


(transient-define-prefix elf-disassemble-prefix ()
  "Disassemble current file"
  [
   ("-f" "symbol" "--disassemble=" :prompt "Select symbol to disassemble: ")
   ("-s" "asm syntax" "--disassembler-options=" :choices (intel att) :always-read t  :allow-empty nil
    :init-value (lambda (obj) (oset obj value "intel")))
   ("d" "Display the assembler mnemonics for the machine instructions from the input file." elf-disassemble-suffix)
   ])

(transient-define-prefix elf-symbols-prefix ()
  "Prefix for showing debug informations"
  [
   ("s" "Displays the entries in symbol table section of the file" (lambda () (interactive) (show-page 'ss)))
   ("d" "Displays the entries in dynamic symbol table section of the file" (lambda () (interactive) (show-page 'sd)))
   ("l" "Displays the contents of any LTO symbol tables in the file." (lambda () (interactive) (show-page 'sl)))
   ])


(transient-define-prefix elf-dispatch ()
  "List of all available transient"
  [ "ELF explorator mode"
    ["Prefix"
     ("g" "Display debug informations" elf-debug-prefix)
     ("s" "Display symbol informations" elf-symbols-prefix)
     ("d" "Disassemble current binary" elf-disassemble-prefix)
     ]

    [ "Single commands"
      ("b" "Show file as binary" (lambda () (interactive) (show-page 'b)))
      ("r" "Show raw file" (lambda () (interactive) (show-page 'raw)))
      ("l" "Prints the shared objects (shared libraries) required by each program" (lambda () (interactive) (show-page 'ldd)))
      ("q" "Quit current ELF file)" (lambda () (interactive) (quit-window)))
      ]

    [
     ("h" "Show main page" (lambda () (interactive) (show-page 'main)))
     ]
    ])

(defvar-keymap elf-mode-map
  "a" (ppage 'all)
  "g" #'elf-debug-prefix
  "d" #'elf-disassemble-prefix
  "s" #'elf-symbols-prefix
  "b" (ppage 'b)
  "l" (ppage 'ldd)
  "r" (ppage 'r)
  "h" (ppage 'h)
  "q" (ppage 'q)
  )

(define-minor-mode elf-mode
  "Minor mode for ELF files."
  :lighter "Elf"
  :keymap elf-mode-map
  (cond
   ((eq (executable-find "objdump") nil) (message "`objdump` not found in PATH. please install it."))
   ((eq (executable-find "readelf") nil) (message "`readelf` not found in PATH. please install it."))
   (t (show-page 'main))))

(add-to-list 'magic-mode-alist '("\^?ELF" . elf-mode))
(add-to-list 'auto-mode-alist '("\\.\\(?:a\\|so\\)\\'" . elf-mode))
(add-to-list 'emulation-mode-map-alists `((elf-mode . ,elf-mode-map)))

(provide 'elf)
