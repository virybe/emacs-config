;; Load path
(push (expand-file-name "site-lisp" user-emacs-directory) load-path)

;; Packages
;; Without this comment Emacs25 adds (package-initialize) here
(require 'package)

(setq package-archives
      '(("gnu"   . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/"))
      package-quickstart t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))


;; Explicitly set the prefered coding systems to avoid annoying prompt
;; from emacs (especially on Microsoft Windows)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(setq-default buffer-file-coding-system 'utf-8)
(setq-default coding-system-for-read 'utf-8)

(setq tramp-debug-buffer t)
;; config on work computer
(cond ((string-equal (system-name) "P200207-VRY-10")
      (setq tramp-default-method "plink")
      ;; (add-to-list 'exec-path "c:/Users/benoit.viry/opt/cmder/vendor/git-for-windows/bin")
      (add-to-list 'exec-path "c:/Users/benoit.viry/opt/cmder/vendor/git-for-windows/usr/bin")
      (add-to-list 'exec-path "C:/ProgramData/chocolatey/bin")
      (add-to-list 'exec-path "C:/Program Files/Zeal")
      (setq default-directory "c:/Users/benoit.viry/")
      (setq explicit-shell-file-name "c:/Windows/System32/bash.exe")
;;      (w32-set-default-process-coding-system)
      (setq python-shell-interpreter "C:/Users/benoit.viry/AppData/Local/Programs/Python/Python38/python.exe"))

      ((string-equal (system-name) "debian")
       (setq python-shell-interpreter "/usr/bin/python3")
       (add-to-list 'exec-path "/home/viry/go/bin")
       )
      )



;; Miscs
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(setq initial-scratch-message nil)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets) ; Show path if names are same
(setq adaptive-fill-regexp "[ t]+|[ t]*([0-9]+.|*+)[ t]*")
(setq adaptive-fill-first-line-regexp "^* *$")
(setq delete-by-moving-to-trash t)         ; Deleting files go to OS's trash folder
(setq make-backup-files nil)               ; Forbide to make backup files
(setq auto-save-default nil)               ; Disable auto save
(setq set-mark-command-repeat-pop t)       ; Repeating C-SPC after popping mark pops it again
(setq-default kill-whole-line t)           ; Kill line including '\n'
(fset 'yes-or-no-p 'y-or-n-p)

(setq-default major-mode 'text-mode)

(setq read-process-output-max (* 1024 1024))
(setq gc-cons-threshold-original gc-cons-threshold)
(setq gc-cons-threshold (* 1024 1024 100))

(setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")
(setq sentence-end-double-space nil)

;; Tab and Space
;; Permanently indent with spaces, never with TABs
(setq-default c-basic-offset   4
              tab-width        4
              indent-tabs-mode nil)

;; UI
(unless (eq window-system 'ns)
  (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))
(blink-cursor-mode -1)

;; Basic modes
(recentf-mode 1)
(setq history-length 25)
(ignore-errors (savehist-mode 1))
(save-place-mode 1)
(show-paren-mode 1)
(electric-pair-mode 0)

(global-goto-address-mode)

;; disable electric pair for '<' in org mode.
;; it corrected the bug when working with SRC block "<s"
(add-hook 'org-mode-hook (lambda ()
           (setq-local electric-pair-inhibit-predicate
                   `(lambda (c)
                  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

(delete-selection-mode 1)

(add-hook 'prog-mode-hook #'subword-mode)
(add-hook 'minibuffer-setup-hook #'subword-mode)

(if (display-graphic-p)
    (add-hook 'prog-mode-hook 'hl-line-mode))
(set-default 'truncate-lines nil)


;; better brace indentation for all c-mode
(setq c-default-style "bsd")
;; I want to have comment start with // in c, no more multiline comments
(add-hook 'c-mode-hook (lambda () (c-toggle-comment-style -1)))

;; compilation
(setq compilation-scroll-output t)

;; debugger
(setq gdb-many-windows nil)


(add-hook 'prog-mode-hook 'display-line-numbers-mode)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CUSTOM MODES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'tmp-snippets)
(require 'elf)
(use-package plz
  :ensure t)
(require 'zoekt-mode)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SMOOTH SCROLL ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(pixel-scroll-precision-mode t)

;; (package-vc-install '(ultra-scroll :vc-backend Git :url  "https://github.com/jdtsmith/ultra-scroll"))


;; (use-package ultra-scroll
;;   ;:load-path "~/code/emacs/ultra-scroll" ; if you git clone'd instead of package-vc-install
;;   :init
;;   (setq scroll-conservatively 101 ; important!
;;         scroll-margin 0) 
;;   :config
;;   (ultra-scroll-mode 1))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CUSTOM MODES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'tmp-snippets)
;; (require 'elf)
;; (require 'zoekt-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IDO ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)
(setq completion-styles '(basic substring partial-completion flex))

;; config from https://git.sr.ht/~ashton314/emacs-bedrock
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Minibuffer and completion
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Vertico: better vertical completion for minibuffer commands
(use-package vertico
  :ensure t
  :init
  ;; You'll want to make sure that e.g. fido-mode isn't enabled
  (vertico-mode))

;; Marginalia: annotations for minibuffer
(use-package marginalia
  :config
  (marginalia-mode))


(use-package eshell
  :init
  (add-hook 'eshell-mode-hook (lambda () (setq pcomplete-cycle-completions nil)))
  (add-hook 'eshell-mode-hook 'compilation-shell-minor-mode)
  (setq eshell-cmpl-cycle-completions nil)
  )



;; please don't ask for reload tags
(setq tags-revert-without-query 1)

(defun revert-current-buffer ()
  "Revert the current buffer."
  (interactive)
  (message "Revert this buffer.")
  (text-scale-set 0)
  (widen)
  (revert-buffer t t))

;; (global-set-key (kbd "<f5>") #'revert-current-buffer)
(global-auto-revert-mode)

(global-set-key (kbd "<f8>") #'gud-next)
(global-set-key (kbd "<f7>") #'gud-step)
(global-set-key (kbd "<f5>") #'gud-run)


;; (global-set-key [remap dabbrev-expand] 'hippie-expand)

(setq inhibit-splash-screen t)

(add-hook 'occur-hook
          (lambda ()
            (save-selected-window
              (pop-to-buffer "*Occur*")
              (fit-window-to-buffer))))

(when (>= emacs-major-version 27)
  (global-so-long-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PERSO ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'project)
(defun my/find-slns ()
  (seq-filter
   (lambda (x)  (string-match-p (regexp-quote "sln") x))
   (project-files (project-current t))))


(defun my/open-vs (sln-file)
  (start-process  "vs" nil "devenv.exe"  sln-file))


(defun my/rebuild-tags ()
  (interactive)
  (progn
    (cd (project-root (project-current t)))
    (start-process "tags" "*ctags*" "ctags.exe" "-e" "--recurse=yes" "--languages=c++,c#")
    (message "tags computed")
    ))

(defun my/restore-nuget (sln-file)
  (progn
    (start-process  "nuget" "*nuget-restore*" "nuget.exe" "restore" "-o" (concat (file-name-directory sln-file) "packages") sln-file)
    (switch-to-buffer-other-window "*nuget-restore*")))

(defun my/project-open-vs ()
  (interactive)
  (let* ((sln-files (my/find-slns)))
    (cond
     ((= (length sln-files) 0) (message "No .sln file found in project"))
     ((= (length sln-files) 1) (my/open-vs (car sln-files)))
     (t (my/open-vs (completing-read "Choose: " sln-files))))))

(defun my/project-restore-nuget ()
  (interactive)
  (let* ((sln-files (my/find-slns)))
    (cond
     ((= (length sln-files) 0) (message "No .sln file found in project"))
     ((= (length sln-files) 1) (my/restore-nuget (car sln-files)))
     (t (my/restore-nuget (completing-read "Choose: " sln-files))))))

(add-to-list 'project-switch-commands '(magit-project-status "Magit" ?m))
(add-to-list 'project-switch-commands '(my/project-open-vs "Open VS" ?V))
(add-to-list 'project-switch-commands '(my/project-open-vs "Restore Nuget" ?n))

(add-to-list 'auto-mode-alist '("\\.csproj\\'" . nxml-mode))
(add-to-list 'auto-mode-alist '("\\.vcxproj\\'" . nxml-mode))


;; some helper function to format characters and integers
(defun hex (x) (format "0x%x" x))
(defun dec (x) (format "%d" x))
(defun ord (c) (format "'%c': %d, 0x%x" c c c))
(defun chr (x) (format "'%c'" x))


(defun my/count-fields ()
  (interactive)
      (let* ((regionp (if (use-region-p) (buffer-substring (region-beginning) (region-end)) (buffer-substring (line-beginning-position) (line-end-position))))
             (fields (split-string (string-trim regionp) ",")))
        (message "%s fields (%s)" (length fields) (string-join fields "|"))))





(defun my/sudo-current-file ()
  (interactive)
  (let ((current-file-name (buffer-file-name)))
    (if current-file-name
        (find-file (concat "/sudo::" current-file-name))
      (message "no file assocciated with the current buffer"))))


(defun my/switch-to-magit-status ()
  (interactive)
  (let ((dir (project-prompt-project-dir)))
    (magit-status dir)))

(defun my/execute-shell ()
  (interactive)
  (async-shell-command (thing-at-point 'line)))

(defun my/new-tab-scratch ()
  (interactive)
  (switch-to-buffer-other-tab "*scratch*"))

(defun my/eshell-other-window (ARG)
  "Open a `eshell' in a new window."
  (interactive "P")
  (let ((buf (eshell ARG)))
    (switch-to-buffer (other-buffer buf))
    (switch-to-buffer-other-window buf)))


(defun switch-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))

(defun switch-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))


(global-unset-key (kbd "C-z"))

(setq-default indent-tabs-mode nil)
(setq save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t
      require-final-newline t
      visible-bell t
      make-pointer-invisible t
      load-prefer-newer t
      ediff-window-setup-function 'ediff-setup-windows-plain
      save-place-file (concat user-emacs-directory "places")
      backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))

;; don't ask to save all buffers when grepping
(setq grep-save-buffers nil)


;; some dired stuff
(when (string-equal (system-name) "P200207-VRY-10")
  (setq ls-lisp-use-insert-directory-program t))

(setq dired-listing-switches "-laGh1v --group-directories-first")

;; when 2 buffers are open side by side, guess the target dir when copying, ...
(setq dired-dwim-target t)

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(setq-default fill-column 100)

(setq org-src-window-setup 'current-window
      org-startup-indented t)

;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((emacs-lisp . t)
;;    (python . t)
;;    (shell . t)
;;    (C . t)
;;    ))

(setq org-confirm-babel-evaluate nil)


(defun align-all-tables ()
  (interactive)
  (org-table-map-tables 'org-table-align))

(add-hook 'org-mode-hook 'align-all-tables)


;; fringes
(fringe-mode '(10 . 0))

;; don't set fringe for bookmarks
(setq bookmark-set-fringe-mark nil)


(defun my/select-n-or-recent (n)
  (if (= (tab-bar--current-tab-index) (- n 1))
      (tab-recent)
    (tab-bar-select-tab n)))

(setq tabbar-use-images nil)
(global-set-key (kbd "M-1") (lambda () (interactive) (my/select-n-or-recent 1)))
(global-set-key (kbd "M-2") (lambda () (interactive) (my/select-n-or-recent 2)))
(global-set-key (kbd "M-3") (lambda () (interactive) (my/select-n-or-recent 3)))
(global-set-key (kbd "M-4") (lambda () (interactive) (my/select-n-or-recent 4)))
(global-set-key (kbd "M-5") (lambda () (interactive) (my/select-n-or-recent 5)))
(global-set-key (kbd "M-6") (lambda () (interactive) (my/select-n-or-recent 6)))
(global-set-key (kbd "M-7") (lambda () (interactive) (my/select-n-or-recent 7)))
(global-set-key (kbd "M-8") (lambda () (interactive) (my/select-n-or-recent 8)))
(global-set-key (kbd "M-9") (lambda () (interactive) (my/select-n-or-recent 9)))
(global-set-key (kbd "C-<tab>") 'tab-recent)
;; Show the tab-bar as soon as tab-bar functions are invoked
(setq tab-bar-show 0)

;; Add the time to the tab-bar, if visible
(add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
(add-to-list 'tab-bar-format 'tab-bar-format-global 'append)
(setq display-time-format "%a %F %T")
(setq display-time-interval 1)
(display-time-mode)

(defun prot/keyboard-quit-dwim ()
  "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
  (interactive)
  (cond
   ((region-active-p)
    (keyboard-quit))
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))
   (t
    (keyboard-quit))))

(define-key global-map (kbd "C-g") #'prot/keyboard-quit-dwim)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PACKAGES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package magit
  :bind
  ("C-x g" . magit-status))

(use-package magit-todos
  :config
  (when (string-equal system-type "windows-nt")
    (setq magit-todos-scanner 'magit-todos--scan-with-git-grep)
    (setq magit-todos-git-grep-extra-args (list "-n"))
    (setq magit-todos-nice nil))
  (magit-todos-mode))

;; (use-package magit-todos
;;   :config
;;   (magit-todos-mode))

(use-package ahk-mode)
(use-package zeal-at-point)
(use-package csv-mode)
(use-package typescript-mode)
(use-package jenkinsfile-mode
  :init
  (add-to-list 'auto-mode-alist '("Build'" . foo-mode))
  )
(use-package dockerfile-mode)
(use-package lox-mode)
(use-package expand-region)
(use-package markdown-mode)
(use-package json-mode)
(use-package nim-mode
  :init
  (require 'nim-compile))

(use-package yaml-mode)

(use-package go-mode
  :config
  (setq godoc-reuse-buffer t)
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook #'gofmt-before-save)
  )


(defun my/setup-tide-mode ()
  (interactive)
  (tide-setup)
  (eldoc-mode +1)
  (company-mode +1)
  (tide-mode))

(use-package tide
  :ensure t
  :init
  (setq tide-tsserver-executable "node_modules/typescript/bin/tsserver")
  :config
  (add-hook 'typescript-mode-hook #'setup-tide-mode))

(use-package restclient
  :init
  (add-to-list 'auto-mode-alist '("\\.http\\'" . restclient-mode)))

(when (< emacs-major-version 29)
    (use-package csharp-mode))

(use-package rust-mode)
(use-package helpful)
(use-package devdocs
  :config
 (add-hook 'c-mode-hook (lambda () (setq-local devdocs-current-docs '("c"))))
 (add-hook 'go-mode-hook (lambda () (setq-local devdocs-current-docs '("go")))))

(use-package rg
  :init
  (when (string-equal (system-name) "P200207-VRY-10")
    (setq rg-executable "/mnt/c/ProgramData/chocolatey/bin/rg.exe")))

(use-package transpose-frame)

(use-package consult
  ;; Other good things to bind: consult-ripgrep, consult-line-multi,
  ;; consult-history, consult-outline
  :bind (("C-x b" . consult-buffer) ; orig. switch-to-buffer
         ("M-y" . consult-yank-pop) ; orig. yank-pop
         )    ; orig. isearch
  :config
  ;; Narrowing lets you restrict results to certain groups of candidates
  (setq consult-narrow-key "<"))

(use-package hl-todo
  :config
  (global-hl-todo-mode))

(use-package which-key
  :config
  (which-key-mode))

(use-package highlight-symbol
  :init
  (setq highlight-symbol-idle-delay 0)
  (add-hook 'prog-mode-hook #'highlight-symbol-mode)
  (add-hook 'text-mode-hook #'highlight-symbol-mode))

(use-package multiple-cursors
  :bind
         (("C-S-c C-S-c" . mc/edit-lines)
         ("C->" . mc/mark-next-like-this)
         ("C-<".  mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this))
  :ensure t)


(when (>= emacs-major-version 29)
  (repeat-mode 1))

(use-package anzu
  :config
  (global-anzu-mode +1))

(use-package general
  :config
  (general-define-key
   "C-<return>" 'set-mark-command
   "C-M-<return>" 'mark-sexp
   )

  (defconst my-leader "C-SPC")

  (general-create-definer my-leader-def
    :prefix my-leader)
  (general-auto-unbind-keys)
  ;; ** Global Keybindings
  (my-leader-def

    "q q" 'save-buffers-kill-terminal

    ;; magit
    "g" 'magit-status

    ;; eshell
    "RET" 'eshell
    "<C-return>" 'my/eshell-other-window

    ;; buffers
    "SPC" 'switch-to-buffer
    "b b" 'switch-to-buffer
    "b o" 'switch-to-buffer-other-window
    ;; "<C-SPC>" 'switch-to-buffer-other-window
    "b k" 'kill-buffer
    "b t" 'switch-to-buffer-other-tab
    "b f" 'switch-to-file-other-tab

    "<C-SPC>" 'perspective-map


    ;; search
    "s r" 'rg
    "s d" 'rg-dwim
    "s p" 'rg-project
    "s o" 'occur
    "s t" 'hl-todo-occur
    "s z" 'zoekt-search-prompt

    ;; error
    "e n" 'next-error
    "e p" 'previous-error
    "e r" 'eglot-rename
    "e f" 'first-error

    ;; windows
    "o"   'other-window
    "O"   '(lambda () (interactive) (other-window -1))
    "w d" 'delete-window
    "w o" 'delete-other-windows
    "w v" 'switch-and-follow-horizontally
    "w h" 'switch-and-follow-vertically
    "w t" 'transpose-frame
    "w s" 'window-swap-states

    ;; compilation
    "c r" 'recompile
    "c c" 'compile
    "c k" 'kill-compilation

    ;; merge
    "m n" 'smerge-next
    "m p" 'smerge-prev
    "m m" 'smerge-keep-mine
    "m o" 'smerge-keep-other

    ;; help
    "h a" 'apropos-command
    "h f" 'helpful-callable
    "h v" 'helpful-variable
    "h k" 'helpful-key
    "h m" 'describe-mode
    "h i" 'info
    "h n" 'view-emacs-news
    "h s" 'shortdoc

    ;; misc
    "'"   'evil-commentary

    ;; project
    "p SPC" 'project-switch-to-buffer
    "p p" 'project-switch-project
    "p f" 'project-find-file
    "p c" 'project-compile
    "p b" 'project-switch-to-buffer
    "p s" 'project-find-regexp
    "p g" 'my/switch-to-magit-status
    "p e" 'project-eshell
    "p r" 'project-query-replace-regexp
    "p v" 'my/project-open-vs
    "p n" 'my/project-restore-nuget
    "p t" 'my/rebuild-tags

    ;; open line
    "n" 'other-frame

    ;; dired
    "d" 'dired

    ;; files
    "f o" 'find-file-other-window
    "f f" 'find-file
    "f r" 'recentf-open-files
    "f t" 'find-file-other-tab

    ;; expand-region
    "i f" 'er/mark-defun
    "i c" 'er/mark-method-call
    "i u" 'er/mark-url
    "i b" 'mark-whole-buffer
    "i '" 'er/mark-comment
    "i s" 'er/mark-symbol
    "i n" 'er/mark-next-accessor

    ;; "a" 'xref-find-definitions -> use 'g d' instead
    "a" 'modus-themes-toggle

    ;; doc Zeal
    ;; "z" 'zeal-at-point
    "z" 'devdocs-lookup

    ;; tab-bar-mode
    "t t" 'my/new-tab-scratch
    "t u" 'tab-bar-undo-close-tab
    "t w" 'tab-close
    "t n" 'tab-next
    "t p" 'tab-previous
    "t b" 'switch-to-buffer-other-tab
    "t d" 'dired-other-tab
    "t r" 'tab-rename
    "t SPC" 'tab-switch
    "t m" 'tab-move
    "t f" 'find-file-other-tab

    ;; yasnippet
    "y n" 'yas-new-snippet
    "y s" 'yas-load-snippet-buffer-and-close

    ;; bookmarks
    "r s" 'bookmark-set
    "r SPC" 'bookmark-jump
    "r d" 'bookmark-delete

    ;; replace at thing
    "<f2>" 'anzu-query-replace-at-cursor-thing
    )
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LSP config ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'project-vc-ignores "./npm/npm-packages-offline-cache/")

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode))

(use-package company
  :ensure t
  :config
  (add-hook 'go-mode-hook 'company-mode))


(use-package eglot
  :config
  (add-to-list 'eglot-server-programs '((c++-mode c-mode) "clangd.exe"))
  (add-to-list 'eglot-server-programs '((go-mode) "gopls.exe"))
  ;; (add-to-list 'eglot-server-programs '((csharp-mode) '("csharp-ls.exe")))

  (add-hook 'go-mode-hook 'eglot-ensure)
  (cond ((string-equal (system-name) "cukby")
         (add-hook 'c-mode-hook 'eglot-ensure)
         (add-hook 'c++-mode-hook 'eglot-ensure)
         (add-hook 'csharp-mode-hook 'eglot-ensure))

        ;; ((string-equal (system-name) "P200207-VRY-10")
        ;;  (add-hook 'c-mode-hook 'eglot-ensure)
        ;;  (add-hook 'c++-mode-hook 'eglot-ensure)
        ;;  (add-hook 'go-mode-hook 'eglot-ensure)
        ;; )
        )
  (add-hook 'eglot-managed-mode-hook (lambda ()
                                       (remove-hook 'flymake-diagnostic-functions 'eglot-flymake-backend)))

  (setq eglot-ignored-server-capabilities '( :hoverProvider
                                             :signatureHelpProvider
                                             :documentHighlightProvider
                                             :documentSymbolProvider
                                             :workspaceSymbolProvider
                                             :codeActionProvider
                                             :codeLensProvider
                                             :documentFormattingProvider
                                             :documentRangeFormattingProvider
                                             :documentOnTypeFormattingProvider
                                             :documentLinkProvider
                                             :colorProvider
                                             :foldingRangeProvider
                                             :executeCommandProvider
                                             :inlayHintProvider))

    )

(global-flycheck-mode -1)


;; (use-package dap-mode
;;   :ensure t
;;   :config
;;   (require 'dap-ui)
;;   (require 'dap-dlv-go)
;;   (dap-mode 1)
;;   (dap-ui-mode 1)

;;   (global-set-key (kbd "<f10>") #'dap-next)
;;   (global-set-key (kbd "<f11>") #'dap-step-in)
;;   (global-set-key (kbd "S-<f11>") #'dap-step-out)
;;   (global-set-key (kbd "<f5>") #'dap-continue)
;;   (global-set-key (kbd "S-<f5>") #'dap-disconnect)
;;   (global-set-key (kbd "<f6>") #'dap-breakpoint-toggle)
;;   (global-set-key (kbd "<f7>") #'dap-ui-expressions-add)
;;   (global-set-key (kbd "S-<f7>") #'dap-ui-expressions-remove)

;;   (setq dap-auto-configure-features
;;         '(locals expressions))

;;   (setq dap-ui-locals-expand-depth t)
;;   (dap-auto-configure-mode)

;;   (setq dap-ui-buffer-configurations
;;         `((,dap-ui--locals-buffer . ((side . right) (slot . 1) (window-width . 0.30)))
;;           (,dap-ui--expressions-buffer . ((side . right) (slot . 2) (window-width . 0.20)))
;;           (,dap-ui--sessions-buffer . ((side . right) (slot . 3) (window-width . 0.20)))
;;           (,dap-ui--breakpoints-buffer . ((side . left) (slot . 2) (window-width . ,treemacs-width)))
;;           (,dap-ui--debug-window-buffer . ((side . bottom) (slot . 3) (window-width . 0.20)))
;;           (,dap-ui--repl-buffer . ((side . right) (slot . 2) (window-width . 0.45)))))

;;   (setq dap-dlv-go-delve-path "/mnt/c/Users/benoit.viry/go/bin/dlv.exe")

;;   )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; from: https://github.com/ianpan870102/.emacs.d/blob/master/config.org
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; clean up whitespaces before save
(use-package whitespace
  :defer t
  :config (add-hook 'before-save-hook 'whitespace-cleanup))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THEME ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq modus-themes-region '(bg-only bg-only))
(setq modus-themes-paren-match '(bold intense))
(setq modus-themes-org-blocks 'rainbow)

(setq modus-themes-headings
      '((1 . (background overline variable-pitch 1.5))
        (2 . (overline rainbow 1.3))
        (3 . (overline 1.1))
        (t . (monochrome))))

(load-theme 'modus-vivendi)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MODLINE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(column-number-mode 1)


;; Startup time
(defun efs/display-startup-time ()
  (message
   "Emacs loaded in %s with %d garbage collections."
   (format
    "%.2f seconds"
    (float-time
     (time-subtract after-init-time before-init-time)))
   gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FONT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun my/set-font (font-name font-size)
  (when (member font-name (font-family-list))
    (set-frame-font (concat font-name "-" (number-to-string font-size)) t t)))

;; other options my linux machine:
;; - Iosevka
;; - JetBrains Mono
;; - DejaVu Sans Mono

(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (my/set-font "Consolas" 14))
 ((string-equal system-type "gnu/linux") ; linux
  (my/set-font "Iosevka" 14)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SCRATCH MESSAGE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(setq initial-scratch-message ";; Scratch buffer\n\n")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CUSTOM MODES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'tmp-snippets)
(require 'elf)
(require 'fasm-mode)
(add-to-list 'auto-mode-alist '("\\.fasm\\'" . fasm-mode))
(add-to-list 'auto-mode-alist '("\\.inc\\'" . fasm-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; config.el ends here
(provide 'config)
